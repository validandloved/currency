if not minetest.get_modpath("loot") then
	return
end

loot.register_loot({
	weights = { generic = 50 },
	payload = {
		stack = ItemStack("currency:valgeld"),
		min_size = 1,
		max_size = 250,
	},
})

loot.register_loot({
	weights = { generic = 50 },
	payload = {
		stack = ItemStack("currency:valgeld_4"),
		min_size = 1,
		max_size = 50,
	},
})

loot.register_loot({
	weights = { generic = 50 },
	payload = {
		stack = ItemStack("currency:valgeld_16"),
		min_size = 1,
		max_size = 10,
	},
})


loot.register_loot({
	weights = { generic = 50 },
	payload = {
		stack = ItemStack("currency:valgeld_64"),
		min_size = 1,
		max_size = 10,
	},
})

loot.register_loot({
	weights = { generic = 50 },
	payload = {
		stack = ItemStack("currency:valgeld_256"),
		min_size = 1,
		max_size = 10,
	},
})
