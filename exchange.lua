local class = futil.class1
local DefaultTable = futil.DefaultTable

local pairs_by_key = futil.table.pairs_by_key

local function get_stack_key(stack, match_meta)
	if match_meta then
		local key_stack = ItemStack(stack) -- clone
		local name = key_stack:get_name()
		local wear = key_stack:get_wear()
		local meta_parts = {}
		for key, value in pairs_by_key(key_stack:get_meta():to_table().fields) do
			table.insert(meta_parts, ("%s=%s"):format(key, value))
		end
		local key_parts = { name }
		if wear > 0 or #meta_parts > 0 then
			table.insert(key_parts, "1")
			table.insert(key_parts, tostring(wear))
		end
		if #meta_parts > 0 then
			table.insert(key_parts, table.concat(meta_parts, ","))
		end

		return table.concat(key_parts, " ")
	else
		return stack:get_name()
	end
end

local function remove_stack_with_meta(inv, list_name, stack)
	local stack_name = stack:get_name()
	local stack_count = stack:get_count()
	local stack_wear = stack:get_wear()
	local stack_meta = stack:get_meta():to_table()
	local list_table = inv:get_list(list_name)

	for _, i_stack in ipairs(list_table) do
		local i_name = i_stack:get_name()
		local i_count = i_stack:get_count()
		local i_wear = i_stack:get_wear()
		local i_meta = i_stack:get_meta():to_table()
		if stack_name == i_name and stack_wear == i_wear and equals(stack_meta, i_meta) then
			if i_count >= stack_count then
				i_count = i_count - stack_count
				stack_count = 0
				i_stack:set_count(i_count)
				break
			else
				stack_count = stack_count - i_count
				i_stack:clear(0)
			end
		end
	end

	inv:set_list(list_name, list_table)

	-- returns the items that were actually removed
	local removed = ItemStack(stack)
	removed:set_count(stack:get_count() - stack_count)
	return removed
end

--------------------

local inv_class = class()

--------------------

function inv_class:_init(inv)
	self.inv = inv
end

--------------------

function inv_class:initialize_inventory()
	-- noop
end

--------------------

function inv_class:get_count(stack, match_meta)
	if type(stack) == "string" then
		stack = ItemStack(stack)
	end
	if stack:is_empty() then
		return 0
	end
	local inv = self.inv
	local total = 0

	local key = get_stack_key(stack, match_meta)
	for _, inv_stack in ipairs(inv:get_list("main")) do
		if key == get_stack_key(inv_stack, match_meta) then
			total = total + inv_stack:get_count()
		end
	end

	return math.floor(total / stack:get_count())
end

function inv_class:get_all_counts(match_meta)
	local inv = self.inv
	local all_counts = DefaultTable(function()
		return 0
	end)

	for _, stack in ipairs(inv:get_list("main")) do
		local key = get_stack_key(stack, match_meta)
		if key ~= "" then
			all_counts[key] = all_counts[key] + stack:get_count()
		end
	end

	return all_counts
end

function inv_class:room_for_item(stack)
	return self.inv:room_for_item("main", stack)
end

function inv_class:add_item(stack)
	return self.inv:add_item("main", stack)
end

function inv_class:contains_item(stack, match_meta)
	return self.inv:contains_item("main", stack, match_meta)
end

function inv_class:remove_item(stack, match_meta)
	local inv = self.inv

	local removed
	if match_meta then
		removed = remove_stack_with_meta(inv, "main", stack)
	else
		removed = inv:remove_item("main", stack)
	end

	return removed
end

function inv_class:get_lists()
	return self.inv:get_lists()
end

--------------------

--[[
function inv_class:get_tmp_inv()
	return smartshop.tmp_inv_class(self.inv)
end

function inv_class:destroy_tmp_inv(tmp_inv)
	tmp_inv:destroy()
end
]]--

--------------------

local player_inv_class = class(inv_class)

--------------------

function player_inv_class:_init(player)
	self.player = player
	self.name = player:get_player_name()
	inv_class._init(self, player:get_inventory())
end

function currency.get_player_inv(player, strict_meta)
	return player_inv_class(player, strict_meta)
end

--------------------

currency.available_currency = {
	["currency:valgeld_1_256"] = 1,
	["currency:valgeld_1_64"] = 4,
	["currency:valgeld_1_16"] = 16,
	["currency:valgeld_1_4"] = 64,
	["currency:valgeld"] = 256,
	["currency:valgeld_4"] = 1024,
	["currency:valgeld_16"] = 4096,
	["currency:valgeld_64"] = 16384,
	["currency:valgeld_256"] = 65536,
	["currency:valgeld_1024"] = 262144,
}

function currency.is_currency(stack)
	if type(stack) == "string" then
		stack = ItemStack(stack)
	end
	local name = stack:get_name()
	return currency.available_currency[name] ~= nil
end

function currency.get_single_value(stack)
	if type(stack) == "string" then
		stack = ItemStack(stack)
	end
	local name = stack:get_name()
	return currency.available_currency[name] or 0
end

function currency.get_stack_value(stack)
	return currency.get_single_value(stack) * stack:get_count()
end

function currency.get_all_currency_in_inv(inv, kind)
	local all_currency = {}
	local all_counts = inv:get_all_counts(kind)
	for item, count in pairs(all_counts) do
		if currency.is_currency(item) then
			while count > 0 do
				local stack = ItemStack(item)
				local maxed_count = math.min(stack:get_stack_max(), count)
				stack:set_count(maxed_count)
				table.insert(all_currency, stack)
				count = count - maxed_count
			end
		end
	end
	-- sort by the value of individual bills, smallest bills first
	table.sort(all_currency, function(a, b)
		return currency.get_single_value(a) < currency.get_single_value(b)
	end)
	return all_currency
end

function currency.get_inv_value(inv, kind)
	local total_value = 0
	local all_currency = currency.get_all_currency_in_inv(inv, kind)
	for i = 1, #all_currency do
		total_value = total_value + currency.get_stack_value(all_currency[i])
	end
	return total_value
end

local function get_change_for_value(value)
	local items = {}
	local largest_first = pairs_by_value(currency.available_currency, function(a, b)
		return b < a
	end)
	for name, currency_value in largest_first do
		if currency_value <= value then
			local count = math.floor(value / currency_value)
			while count > 0 do
				local stack = ItemStack(name)
				local maxed_count = math.min(stack:get_stack_max(), count)
				stack:set_count(maxed_count)
				table.insert(items, stack)
				value = value - (currency_value * maxed_count)
				count = count - maxed_count
			end
			if value == 0 then
				break
			end
		end
	end
	if value > 0 then
		--smartshop.util.error("currency changing: some value (%s) is remaining %s", value)
	end
	return items
end

function currency.remove_item(inv, stack, kind)
	local owed_value = currency.get_stack_value(stack)
	local inv_total_value = currency.get_inv_value(inv, kind)
	if owed_value > inv_total_value then
		return ItemStack()
	end

	local all_currency = currency.get_all_currency_in_inv(inv, kind)
	local i = 1
	while owed_value > 0 do
		local currency_stack = all_currency[i]
		local value = currency.get_stack_value(currency_stack)
		if value <= owed_value then
			inv:remove_item(currency_stack)
			owed_value = owed_value - value
			i = i + 1
		else
			local item_value = currency.get_single_value(currency_stack)
			local number_to_remove = math.ceil(owed_value / item_value)
			local stack_to_remove = ItemStack(currency_stack)
			stack_to_remove:set_count(number_to_remove)
			inv:remove_item(stack_to_remove)
			owed_value = owed_value - currency.get_stack_value(stack_to_remove)
			break
		end
	end

	if owed_value < 0 then
		local to_refund = get_change_for_value(-owed_value)
		for _, refund_stack in ipairs(to_refund) do
			local remainder = inv:add_item(refund_stack)
			if not remainder:is_empty() then
				return ItemStack()
			end
		end
	end

	return ItemStack(stack)
end

minetest.register_on_joinplayer(function (player)
	local inv = currency.get_player_inv(player)
	print(currency.get_inv_value(inv))
end)
