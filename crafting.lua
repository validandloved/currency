if minetest.get_modpath("default") then
	minetest.register_craft({
		output = "currency:safe",
		recipe = {
			{"default:steel_ingot", "default:steel_ingot",
				"default:steel_ingot"},
			{"default:steel_ingot", "default:mese_crystal",
				"default:steel_ingot"},
			{"default:steel_ingot", "default:steel_ingot",
				"default:steel_ingot"},
		}
	})

	minetest.register_craft({
		output = "currency:shop",
		recipe = {
			{"default:sign_wall"},
			{"default:chest_locked"},
		}
	})

	minetest.register_craft({
		output = "currency:barter",
		recipe = {
			{"default:sign_wall"},
			{"default:chest"},
		}
	})
end

minetest.register_craft({
	type = "shapeless",
	output = "currency:valgeld_256 4",
	recipe = {
		"currency:valgeld_1024",
	},
})

minetest.register_craft({
	type = "shapeless",
	output = "currency:valgeld_64 4",
	recipe = {
		"currency:valgeld_256",
	},
})

minetest.register_craft({
	type = "shapeless",
	output = "currency:valgeld_16 4",
	recipe = {
		"currency:valgeld_64",
	},
})

minetest.register_craft({
	type = "shapeless",
	output = "currency:valgeld_4 4",
	recipe = {
		"currency:valgeld_16",
	},
})

minetest.register_craft({
	type = "shapeless",
	output = "currency:valgeld 4",
	recipe = {
		"currency:valgeld_4",
	},
})

minetest.register_craft({
	type = "shapeless",
	output = "currency:valgeld_1_4 4",
	recipe = {
		"currency:valgeld",
	},
})

minetest.register_craft({
	type = "shapeless",
	output = "currency:valgeld_1_16 4",
	recipe = {
		"currency:valgeld_1_4",
	},
})

minetest.register_craft({
	type = "shapeless",
	output = "currency:valgeld_1_64 4",
	recipe = {
		"currency:valgeld_1_16",
	},
})

minetest.register_craft({
	type = "shapeless",
	output = "currency:valgeld_1_256 4",
	recipe = {
		"currency:valgeld_1_64",
	},
})

minetest.register_craft({
	type = "shapeless",
	output = "currency:valgeld_1_64",
	recipe = {
		"currency:valgeld_1_256",
		"currency:valgeld_1_256",
		"currency:valgeld_1_256",
		"currency:valgeld_1_256",
	},
})

minetest.register_craft({
	type = "shapeless",
	output = "currency:valgeld_1_16",
	recipe = {
		"currency:valgeld_1_64",
		"currency:valgeld_1_64",
		"currency:valgeld_1_64",
		"currency:valgeld_1_64",
	},
})

minetest.register_craft({
	type = "shapeless",
	output = "currency:valgeld_1_4",
	recipe = {
		"currency:valgeld_1_16",
		"currency:valgeld_1_16",
		"currency:valgeld_1_16",
		"currency:valgeld_1_16",
	},
})

minetest.register_craft({
	type = "shapeless",
	output = "currency:valgeld",
	recipe = {
		"currency:valgeld_1_4",
		"currency:valgeld_1_4",
		"currency:valgeld_1_4",
		"currency:valgeld_1_4",
	},
})

minetest.register_craft({
	type = "shapeless",
	output = "currency:valgeld_4",
	recipe = {
		"currency:valgeld",
		"currency:valgeld",
		"currency:valgeld",
		"currency:valgeld",
	},
})

minetest.register_craft({
	type = "shapeless",
	output = "currency:valgeld_16",
	recipe = {
		"currency:valgeld_4",
		"currency:valgeld_4",
		"currency:valgeld_4",
		"currency:valgeld_4",
	},
})

minetest.register_craft({
	type = "shapeless",
	output = "currency:valgeld_64",
	recipe = {
		"currency:valgeld_16",
		"currency:valgeld_16",
		"currency:valgeld_16",
		"currency:valgeld_16",
	},
})

minetest.register_craft({
	type = "shapeless",
	output = "currency:valgeld_256",
	recipe = {
		"currency:valgeld_64",
		"currency:valgeld_64",
		"currency:valgeld_64",
		"currency:valgeld_64",
	},
})

minetest.register_craft({
	type = "shapeless",
	output = "currency:valgeld_1024",
	recipe = {
		"currency:valgeld_256",
		"currency:valgeld_256",
		"currency:valgeld_256",
		"currency:valgeld_256",
	},
})

-- Temporary fix for item aliasing engine bug

minetest.register_craft({
	type = "shapeless",
	output = "currency:valgeld_1_256",
	recipe = {
		"currency:minegeld_1_256",
	},
})

minetest.register_craft({
	type = "shapeless",
	output = "currency:valgeld_1_64",
	recipe = {
		"currency:minegeld_1_64",
	},
})

minetest.register_craft({
	type = "shapeless",
	output = "currency:valgeld_1_16",
	recipe = {
		"currency:minegeld_1_16",
	},
})

minetest.register_craft({
	type = "shapeless",
	output = "currency:valgeld_1_4",
	recipe = {
		"currency:minegeld_1_4",
	},
})

minetest.register_craft({
	type = "shapeless",
	output = "currency:valgeld",
	recipe = {
		"currency:minegeld",
	},
})

minetest.register_craft({
	type = "shapeless",
	output = "currency:valgeld_4",
	recipe = {
		"currency:minegeld_4",
	},
})

minetest.register_craft({
	type = "shapeless",
	output = "currency:valgeld_16",
	recipe = {
		"currency:minegeld_16",
	},
})

minetest.register_craft({
	type = "shapeless",
	output = "currency:valgeld_64",
	recipe = {
		"currency:minegeld_64",
	},
})

minetest.register_craft({
	type = "shapeless",
	output = "currency:valgeld_256",
	recipe = {
		"currency:minegeld_256",
	},
})

minetest.register_craft({
	type = "shapeless",
	output = "currency:valgeld_1024",
	recipe = {
		"currency:minegeld_1024",
	},
})

