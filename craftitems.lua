local S = minetest.get_translator("currency")

-- alias for backwards compatibility with older versions
-- aliasing is broken right now, see temp fix below
--minetest.register_alias_force("currency:minegeld_1_256", "currency:valgeld_1_256")
minetest.register_craftitem("currency:valgeld_1_256", {
	description = S("@1 VALgeld Coin", "1/256"),
	inventory_image = "minegeld_1_256.png",
		stack_max = 32768,
		groups = {valgeld = 1, valgeld_coin = 1}
})

--minetest.register_alias_force("currency:minegeld_1_64", "currency:valgeld_1_64")
minetest.register_craftitem("currency:valgeld_1_64", {
	description = S("@1 VALgeld Coin", "1/64"),
	inventory_image = "minegeld_1_64.png",
		stack_max = 32768,
		groups = {valgeld = 1, valgeld_coin = 1}
})

--minetest.register_alias_force("currency:minegeld_1_16", "currency:valgeld_1_16")
minetest.register_craftitem("currency:valgeld_1_16", {
	description = S("@1 VALgeld Coin", "1/16"),
	inventory_image = "minegeld_1_16.png",
		stack_max = 32768,
		groups = {valgeld = 1, valgeld_coin = 1}
})

--minetest.register_alias_force("currency:minegeld_1_4", "currency:valgeld_1_4")
minetest.register_craftitem("currency:valgeld_1_4", {
	description = S("@1 VALgeld Coin", "1/4"),
	inventory_image = "minegeld_1_4.png",
		stack_max = 32768,
		groups = {valgeld = 1, minegeld_coin = 1}
})

--minetest.register_alias_force("currency:minegeld", "currency:valgeld")
minetest.register_craftitem("currency:valgeld", {
	description = S("@1 VALgeld Note", "1"),
	inventory_image = "minegeld.png",
		stack_max = 32768,
		groups = {valgeld = 1, valgeld_note = 1}
})

--minetest.register_alias_force("currency:minegeld_4", "currency:valgeld_4")
minetest.register_craftitem("currency:valgeld_4", {
	description = S("@1 VALgeld Note", "4"),
	inventory_image = "minegeld_4.png",
		stack_max = 32768,
		groups = {valgeld = 1, valgeld_note = 1}
})

--minetest.register_alias_force("currency:minegeld_16", "currency:valgeld_16")
minetest.register_craftitem("currency:valgeld_16", {
	description = S("@1 VALgeld Note", "16"),
	inventory_image = "minegeld_16.png",
		stack_max = 32768,
		groups = {valgeld = 1, valgeld_note = 1}
})

--minetest.register_alias_force("currency:minegeld_64", "currency:valgeld_64")
minetest.register_craftitem("currency:valgeld_64", {
	description = S("@1 VALgeld Note", "64"),
	inventory_image = "minegeld_64.png",
		stack_max = 32768,
		groups = {valgeld = 1, valgeld_note = 1}
})

--minetest.register_alias_force("currency:minegeld_256", "currency:valgeld_256")
minetest.register_craftitem("currency:valgeld_256", {
	description = S("@1 VALgeld Note", "256"),
	inventory_image = "minegeld_256.png",
		stack_max = 32768,
		groups = {valgeld = 1, valgeld_note = 1}
})

--minetest.register_alias_force("currency:minegeld_1024", "currency:valgeld_1024")
minetest.register_craftitem("currency:valgeld_1024", {
	description = S("@1 VALgeld Note", "1024"),
	inventory_image = "minegeld_1024.png",
		stack_max = 32768,
		groups = {valgeld = 1, valgeld_note = 1}
})

-- Temp fix for aliasing

minetest.register_craftitem("currency:minegeld_1_256", {
	description = S("PUT ME IN CRAFTING GRID!"),
	inventory_image = "minegeld_1_256.png",
		stack_max = 32768,
		groups = {valgeld = 1, valgeld_coin = 1}
})

minetest.register_craftitem("currency:minegeld_1_64", {
	description = S("PUT ME IN CRAFTING GRID!"),
	inventory_image = "minegeld_1_64.png",
		stack_max = 32768,
		groups = {valgeld = 1, valgeld_coin = 1}
})

minetest.register_craftitem("currency:minegeld_1_16", {
	description = S("PUT ME IN CRAFTING GRID"),
	inventory_image = "minegeld_1_16.png",
		stack_max = 32768,
		groups = {valgeld = 1, valgeld_coin = 1}
})

minetest.register_craftitem("currency:minegeld_1_4", {
	description = S("PUT ME IN CRAFTING GRID!"),
	inventory_image = "minegeld_1_4.png",
		stack_max = 32768,
		groups = {valgeld = 1, minegeld_coin = 1}
})

minetest.register_craftitem("currency:minegeld", {
	description = S("PUT ME IN CRAFTING GRID!"),
	inventory_image = "minegeld.png",
		stack_max = 32768,
		groups = {valgeld = 1, valgeld_note = 1}
})

minetest.register_craftitem("currency:minegeld_4", {
	description = S("PUT ME IN CRAFTING GRID!"),
	inventory_image = "minegeld_4.png",
		stack_max = 32768,
		groups = {valgeld = 1, valgeld_note = 1}
})

minetest.register_craftitem("currency:minegeld_16", {
	description = S("PUT ME IN CRAFTING GRID!"),
	inventory_image = "minegeld_16.png",
		stack_max = 32768,
		groups = {valgeld = 1, valgeld_note = 1}
})

minetest.register_craftitem("currency:minegeld_64", {
	description = S("PUT ME IN CRAFTING GRID!"),
	inventory_image = "minegeld_64.png",
		stack_max = 32768,
		groups = {valgeld = 1, valgeld_note = 1}
})

minetest.register_craftitem("currency:minegeld_256", {
	description = S("PUT ME IN CRAFTING GRID!"),
	inventory_image = "minegeld_256.png",
		stack_max = 32768,
		groups = {valgeld = 1, valgeld_note = 1}
})

minetest.register_craftitem("currency:minegeld_1024", {
	description = S("PUT ME IN CRAFTING GRID!"),
	inventory_image = "minegeld_1024.png",
		stack_max = 32768,
		groups = {valgeld = 1, valgeld_note = 1}
})

